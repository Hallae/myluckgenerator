﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
namespace MyluckGenerator
{
    internal class Program
    {
        private static Random random = new Random();

        private static int GetUserInput()
        {


            int GameType;
            Console.Write("\n\n");

            Console.Write("My own personal lucky stoloto lottery jackpot generator:\n");
            Console.Write("-------------------------------------------------------------------------");
            Console.Write("\n\n");
            Console.Write("Input 1 for Rapido\n");
            Console.Write("Input 2 for numbers of 4x20\n");
            Console.Write("Input 3 for numbers of rapido 2.0\n");
            Console.Write("Input 4 for numbers of Two times two\n");
            Console.WriteLine("Input 5 to generate commonly played numbers\n");
            Console.WriteLine("Input 6 to exit\n");

            Console.WriteLine("Do you want to open the link in your default browser ? (y/n): ");
            string input = Console.ReadLine();

            if (input.ToLower() == "y")
            {
                System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo
                {
                    FileName = "https://www.stoloto.ru/",
                    UseShellExecute = true
                });
            }

            int.TryParse(input, out GameType);
            return GameType;


        }
        static void Main(string[] args)
        {
            int[] availableNumbers = { 5, 13, 18, 19, 20, 10, 15, 3, 4, 6, 7, 8, 9, 11, 17, 6, 16, 2, 10 };


            while (true)
            {
                int GameType = GetUserInput();


                switch (GameType)
                {
                    case 1:
                    case 3:
                        var (numbers, extraNumber) = GenerateRandomCombination();
                        Console.WriteLine("Random combination: " + string.Join(", ", numbers));
                        Console.WriteLine("Extra number: " + extraNumber);
                        break;
                    case 2:
                        (numbers, extraNumber) = GenerateRandomCombination();
                        Console.WriteLine("Random combination: " + string.Join(", ", numbers));
                        break;
                    case 5:
                        numbers = GenerateRandomCombinationWithRepeats();
                        Console.WriteLine("Random combination: " + string.Join(", ", numbers));
                        Console.WriteLine("Extra number: " + 3 + " or " + 4);
                        Console.WriteLine("3 and 4 are the most commonly played bonus numbers, so I recommend using 3 or 4.");
                        break;
                    case 4:
                        int[] quadNumbers = new int[4];
                        for (int i = 0; i < 4; i++)
                        {
                            quadNumbers[i] = random.Next(4, 27);
                        }
                        Console.WriteLine("Random quad numbers: " + string.Join(", ", quadNumbers));
                        break;
                    case 6:
                        Console.WriteLine("Thank you for using MyLuckGenerator!");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Invalid GameType value.");
                        break;
                }

            }

        }




        static (List<int>, int) GenerateRandomCombination()
        {
            int[] availableNumbers = { 5, 13, 18, 19, 20, 10, 15, 3, 4, 6, 7, 8, 9, 11, 17, 6, 16, 2, 10 };
            var random = new Random();
            var numbers = new List<int>();
            // Shuffle the available numbers
            for (int i = availableNumbers.Length - 1; i > 0; i--)
            {
                int j = random.Next(i + 1);
                int temp = availableNumbers[i];
                availableNumbers[i] = availableNumbers[j];
                availableNumbers[j] = temp;
            }
            // Select the first 8 numbers from the shuffled list
            for (int i = 0; i < 8; i++)
            {
                numbers.Add(availableNumbers[i]);
            }
            int[] quadNumbers = new int[4];
            for (int i = 0; i < 4; i++)
            {
                quadNumbers[i] = random.Next(4, 27);
            }
            int extraNumber = random.Next(1, 5); // Generate a random extra number from the list up to 4
            return (numbers, extraNumber);
        }
        static List<int> GenerateRandomCombinationWithRepeats()
        {
            int[] availableNumbers = { 5, 13, 18, 19, 20, 10, 15, 3, 4, 6, 7, 8, 9, 11, 17, 6, 16, 2, 10 };
            var random = new Random();
            var repeatedNumbers = new List<int>();
            // Populate the repeatedNumbers list with the desired repeating pattern
            for (int i = 0; i < availableNumbers.Length; i++)
            {
                repeatedNumbers.Add(availableNumbers[i]);
                if ((i + 1) % 4 == 0)
                {
                    repeatedNumbers.Add(availableNumbers[i]);
                }
            }
            // Shuffle the repeatedNumbers list using the Fisher-Yates shuffle algorithm
            for (int i = repeatedNumbers.Count - 1; i > 0; i--)
            {
                int j = random.Next(i + 1);
                int temp = repeatedNumbers[i];
                repeatedNumbers[i] = repeatedNumbers[j];
                repeatedNumbers[j] = temp;
            }
            // Select the first 8 numbers from the shuffled list
            var numbers = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                numbers.Add(repeatedNumbers[i]);
            }
            return numbers;
        }


    }
}